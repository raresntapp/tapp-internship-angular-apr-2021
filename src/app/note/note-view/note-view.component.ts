import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note } from '../../note';

@Component({
  selector: 'app-note-view',
  templateUrl: './note-view.component.html',
  styleUrls: ['./note-view.component.scss']
})
export class NoteViewComponent implements OnInit {

  @Input() note: Note;
  @Input() id: number;
  @Output() noteClick = new EventEmitter<number>();

  clicks: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    this.clicks++;
    this.noteClick.emit(this.clicks);
  }

}
