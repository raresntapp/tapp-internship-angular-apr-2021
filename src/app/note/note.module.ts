import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteViewComponent } from './note-view/note-view.component';
import { NotePageComponent } from './note-page/note-page.component';
import { NotesListComponent } from './notes-list/notes-list.component';
import { FormsModule } from '@angular/forms';
import { NoteRoutingModule } from './note-routing.module';



@NgModule({
  declarations: [
    NotesListComponent,
    NotePageComponent,
    NoteViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NoteRoutingModule
  ]
})
export class NoteModule { }
