import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Note } from '../../note';
import { NoteService } from '../../note.service';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {

  notes: Note[];
  newNote: string = 'New note';

  constructor(private noteService: NoteService, private router : Router) {}

  ngOnInit() {
    this.loadData();
  }

  onNoteClick(clicks: number, note: Note) {
    this.router.navigate(['notes', note.id])

  }

  async loadData(){
    this.notes = await this.noteService.getNotes();
  }

}
