import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Note } from '../../note';
import { NoteService } from '../../note.service';

@Component({
  selector: 'app-note-page',
  templateUrl: './note-page.component.html',
  styleUrls: ['./note-page.component.scss']
})
export class NotePageComponent implements OnInit {
  note: Note;
  noteId: number;

  constructor(private activatedRoute : ActivatedRoute, private noteService: NoteService) { }

  ngOnInit(): void {
    this.noteId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    console.log(this.noteId);
    this.loadData();
  }

  async loadData(){
    this.note = await this.noteService.getNoteById(this.noteId);
  }

  saveNote(event: any, note: Partial<Note>){
    console.log(note);
  }

}
