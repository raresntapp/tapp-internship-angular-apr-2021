import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Note } from './note';

@Injectable({
  providedIn: 'root',
})
export class NoteService {

  constructor(private http: HttpClient) {}

  async getNotes() {
    return this.http.get<Note[]>('https://jsonplaceholder.typicode.com/todos').toPromise();
  }

  async getNoteById(id: number){
    // return this.notes.find(note => note.id === id);
    return this.http.get<Note>(`https://jsonplaceholder.typicode.com/todos/${id}`).toPromise();
  }
}
